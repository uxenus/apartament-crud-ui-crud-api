const calendarSelector = '#calendar';

$(function () { // dom ready

    var todayDate = moment().startOf('day');
    var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
    var TODAY = todayDate.format('YYYY-MM-DD');
    var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

    $(calendarSelector).fullCalendar({
        // locale: 'ru',
        resourceAreaWidth: 230,
        editable: true,
        aspectRatio: 1.7,
        scrollTime: '00:00',
        // slotDuration: {days: 10},
        slotLabelInterval: {days: 1},
        businessHours: true,

        themeSystem: 'standard',

        nowIndicator: true,

        header: {
            left: 'promptResource,addInterval today prev,next',
            center: 'title',
            right: 'timelineMonth,timelineYear,listWeek'
        },

        defaultView: 'timelineMonth',
        // defaultView: 'timelineMonth',
        views: {
            timelineThreeDays: {
                type: 'timeline',
                duration: {days: 7},
                dayCount: 1
            }
        },
        resourceLabelText: '- Апартаменты -',
        
        selectable: true,
        eventDrop: function(event, delta, revertFunc) {

            console.log(event);


            let formData = new FormData();
            formData.append('date_start', new Date(Date.parse(event.start)).toISOString().slice(0, 10));
            formData.append('date_end', new Date(Date.parse(event.end)).toISOString().slice(0, 10));
            formData.append('room_id', event.resourceId);
            formData.append('intervalId', event.id);

            console.log(formData);


            let url_for_delete = '/api/interval/' + event.id;

            fetch(url_for_delete, {
                method: 'POST',
                headers: {
                    'Username': 'api-test-user',
                    'Userpassword': 'api-test-password',
                },
                body: formData
            })
                .then(function (resp) {
                    if (resp.status !== 200) {
                        console.log('Произошла ошибка при сохранении интервала. Status Code: #1 ' + resp.status);
                        revertFunc();
                        return;
                    }
                    resp.json().then(function (resp) {
                        console.log('saveInterval:', resp);
                        if (resp.status !== false) {

                        } else {
                            revertFunc();
                            console.log('Произошла ошибка при сохранении интервала. Status Code: #2 ' + resp.status);
                        }
                    })
                });
        },
        selectOverlap: function(event) {
            return event.rendering === 'background';
        },
        select: function(startDate, endDate, jsEvent, view, resource) {
            let selectedData = JSON.stringify({start: startDate.format(), end: endDate.format(), roomId: resource.id});
            window.localStorage.setItem('intervalSelected', selectedData);

            ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'].forEach(function (val) {
                let thisSelector = '#ADDintervalDaysContainer #interval_day_' + val;
                $(thisSelector).removeAttr('checked');
                $(thisSelector).val(0);
            });

            $('#addEventModal').modal();
        },
        resourceRender: function(resourceObj, labelTds, bodyTds) {
            labelTds.on('click', function(){
                window.localStorage.setItem('roomToShow', JSON.stringify(resourceObj));
                $('#showRoomModal').modal();
            });
        },
        eventClick: function (calEvent) {

            console.log('eventClick' , calEvent);

            let DATA_editInterval = {
                _id: calEvent._id,
                id: calEvent.id,
                title: calEvent.title,
                price: calEvent.price,
                resourceId: calEvent.resourceId,
                start: calEvent.start ? calEvent.start : calEvent.end,  // fix bug of plugin with null value when same Start and End date
                end: calEvent.end ? calEvent.end : calEvent.start, // fix bug of plugin with null value when same Start and End date
                mon: calEvent.mon,
                tue: calEvent.tue,
                wed: calEvent.wed,
                thu: calEvent.thu,
                fri: calEvent.fri,
                sat: calEvent.sat,
                sun: calEvent.sun
            };

            window.localStorage.setItem('DATA_editInterval', JSON.stringify(DATA_editInterval));

            $('#editEventModal').modal();
        },

        /* --------- */
        customButtons: {
            promptResource: {
                text: '+ Апартаменты',
                click: function () {

                    $('#addRoomModal').modal();
                }
            },
            addInterval: {
                text: '+ Интервал цены',
                click: function () {

                    $('#addEventModal').modal();

                }
            }
        },
        /* ---------- */
        resources: function (resourceFunc) {
            fetch('/api/rooms', {
                method: 'POST',
                headers: {
                    'Username': 'api-test-user',
                    'Userpassword': 'api-test-password',
                }
            })
                .then(function (resp) {
                    if (resp.status !== 200) {
                        alert('Произошла ошибка загрузке Апартаментов');
                        console.log('Произошла ошибка загрузке Апартаментов. Status Code: #1 ' + resp.status);
                        return;
                    }
                    resp.json().then(function (data) {
                        console.log(data.roomsData);
                        if (data.status !== false) {
                            resourceFunc(data.roomsData);
                        } else {
                            alert('Произошла ошибка загрузке Апартаментов');
                            console.log('Произошла ошибка загрузке Апартаментов. Status Code: #2 ' + data.status);
                        }
                    })
                });
        },
        events: function (start, end, timezone, eventsFunc) {
            let formData = new FormData();
            formData.append('start', new Date(Date.parse(start)).toISOString().slice(0, 10));
            formData.append('end', new Date(Date.parse(end)).toISOString().slice(0, 10));

            fetch('/api/intervals', {
                method: 'POST',
                headers: {
                    'Username': 'api-test-user',
                    'Userpassword': 'api-test-password',
                },
                body: formData
            })
                .then(function (resp) {
                    if (resp.status !== 200) {
                        alert('Произошла ошибка загрузки интервалов цен');
                        console.log('Произошла ошибка загрузки интервалов цен. Status Code: #1 ' + resp.status);
                        return;
                    }
                    resp.json().then(function (data) {
                        console.log(data);
                        if(data.status !== false) {
                            eventsFunc(data.intervalsData);
                        } else {
                            // alert('Произошла ошибка загрузки интервалов цен');
                            console.log('Произошла ошибка загрузки интервалов цен. Status Code: #2 ' + data.status);
                        }
                    })
                });
        },
        eventOverlap: function(stillEvent, movingEvent) {

            return stillEvent.allDay && movingEvent.allDay;
        }

    });

});

/* ============================================= */

// readjust sizing after font load
$(window).on('load', function () {
    $(calendarSelector).fullCalendar('render');

    /**
     * Delete price Interval
     */

    $('#editEventModal #intervalDeleteBtn').on('click', function () {
        let data = window.localStorage.getItem('DATA_editInterval');
        if (data) {
            data = JSON.parse(data);

            let url_for_delete = '/api/interval/' + data.id;

            fetch(url_for_delete, {
                method: 'DELETE',
                headers: {
                    'Username': 'api-test-user',
                    'Userpassword': 'api-test-password',
                }
            })
                .then(function (resp) {
                    if (resp.status !== 200) {
                        alert('Произошла ошибка при удалении интервала.');
                        console.log('Произошла ошибка при удалении интервала. Status Code: #1 ' + resp.status);
                        return;
                    }
                    resp.json().then(function (res) {
                        console.log('deleteInterval:', res);
                        if (res.status !== false) {
                            window.localStorage.setItem('DATA_editInterval', '');

                            $(calendarSelector).fullCalendar('removeEvents', res.deletedId);

                        } else {
                            alert('Произошла ошибка при удалении интервала.');
                            console.log('Произошла ошибка при удалении интервала. Status Code: #2 ' + res.status);
                        }
                    })
                });
        }
    });

    /**
     * Edit / Update price interval
     */

    $('#editEventModal #intervalSaveBtn').on('click', function () {

        console.log('start save interval');

        let data = window.localStorage.getItem('DATA_editInterval');
        if (data) {
            data = JSON.parse(data);

            let bodyData = new FormData(document.forms.editintervalform);
            let arrayData = {};

            bodyData.forEach((data, index) => {
                arrayData[index] = data;
            });

            arrayData['_id'] = data._id;
            arrayData['id'] = data.id;
            arrayData['allDay'] = true;
            arrayData['resourceId'] = parseInt(data.resourceId);

            let url_for_delete = '/api/interval/' + data.id;
                                                                    
            fetch(url_for_delete, {
                method: 'POST',
                headers: {
                    'Username': 'api-test-user',
                    'Userpassword': 'api-test-password',
                },
                body: bodyData
            })
                .then(function (resp) {
                    if (resp.status !== 200) {
                        alert('Произошла ошибка при сохранении интервала.');
                        console.log('Произошла ошибка при сохранении интервала. Status Code: #1 ' + resp.status);
                        return;
                    }
                    resp.json().then(function (resp) {
                        console.log('saveInterval:', resp);
                        if (resp.status !== false) {
                            window.localStorage.setItem('DATA_editInterval', '');

                            arrayData['id'] = parseInt(resp.updatedId);
                            arrayData['start'] = moment(arrayData['date_start']);
                            arrayData['end'] = moment(arrayData['date_end']);

                            $(calendarSelector).fullCalendar('removeEvents', arrayData.id);
                            $(calendarSelector).fullCalendar('renderEvent', arrayData);
                            $(calendarSelector).fullCalendar('rerenderEvents');

                        } else {
                            if(resp.msg) {
                                alert('Произошла ошибка: ' + resp.msg);
                            } else {
                                alert('Произошла ошибка при сохранении интервала.');
                            }
                            console.log('Произошла ошибка при сохранении интервала. Status Code: #2 ' + resp.status);
                        }
                    })
                });
        }
    });

    /**
     * Show Modal for Edit / Update price interval
     */

    $('#editEventModal').on('shown.bs.modal', function () {

        let data = window.localStorage.getItem('DATA_editInterval');

        if(data) {
            data = JSON.parse(data);

            let data_start = new Date(Date.parse(data.start)).toISOString().slice(0, 10);
            let data_end = new Date(Date.parse(data.end)).toISOString().slice(0, 10);
            let RoomData = $(calendarSelector).fullCalendar('getResourceById', parseInt(data.resourceId));

            let allRoomsData = $(calendarSelector).fullCalendar('getResources');

            let roomsSelectObj = $('<select />', { class: 'custom-select col-12', id: 'room_id', name: 'room_id' });
            $('<option />', {value: '', text: 'Сделайте выбор...'}).appendTo(roomsSelectObj);

            allRoomsData.forEach(function (val) {
                let optionText = '№' + val.number + ' - ' + val.title;
                $('<option />', {value: val.id, text: optionText}).appendTo(roomsSelectObj)
            });
            roomsSelectObj.val(parseInt(data.resourceId));

            $('#forRoomSelect').html(roomsSelectObj);

            $('#editEventModal #RoomNameH5').text(RoomData.number);
            $('#editEventModal #editEventName').val(data.title);
            $('#editEventModal #editEventIntervalStart').val(data_start);
            $('#editEventModal #editEventIntervalEnd').val(data_end);
            $('#editEventModal #editEventCost').val(data.price);

            $('#intervalDaysContainer input[type=checkbox]').css('background-image', '');

            ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'].forEach(function (val) {
                let thisSelector = '#intervalDaysContainer #interval_day_' + val;

                if (parseInt(data[val]) === 1) {
                    $(thisSelector).attr('checked', 'checked');
                    $(thisSelector).val(1);
                } else {
                    $(thisSelector).removeAttr('checked');
                    $(thisSelector).val(0);
                }
            });

        }
    });

    /**
     * Show Modal for creating new price Interval
     */

    $('#addEventModal').on('shown.bs.modal', function () {
        let selectedData = JSON.parse(window.localStorage.getItem('intervalSelected'));

        let allRoomsData = $(calendarSelector).fullCalendar('getResources');
        let roomsSelectObj = $('<select />', {class: 'custom-select col-12', id: 'ADDroom_id', name: 'room_id'});

        $('<option />', {value: '', text: 'Сделайте выбор...'}).appendTo(roomsSelectObj);

        allRoomsData.forEach(function (val) {
            let optionText = '№' + val.number + ' - ' + val.title;
            $('<option />', {value: val.id, text: optionText}).appendTo(roomsSelectObj)
        });
        roomsSelectObj.val(selectedData.roomId);

        $('#addEventModal #ADDEventIntervalStart').val(selectedData.start);
        $('#addEventModal #ADDEventIntervalEnd').val(selectedData.end);
        $('#addEventModal #addEventName').val('');
        $('#addEventModal #addEventCost').val('');

        $('#ADDforRoomSelect').html(roomsSelectObj);

        ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'].forEach(function (val) {
            let thisSelector = '#ADDintervalDaysContainer #interval_day_' + val;

            $(thisSelector).removeAttr('checked');
            $(thisSelector).val(0);
            console.log($(thisSelector).closest('.custom-control-indicator'));
            $(thisSelector).closest('.custom-control-indicator').css('background-image', '');
        });

    });

    /**
     * Show Modal for View Info about Apartment
     */

    $('#showRoomModal').on('shown.bs.modal', function () {
        let selectedData = JSON.parse(window.localStorage.getItem('roomToShow'));

        $('#showRoomModal #showRoomName').val(selectedData.title);
        $('#showRoomModal #showRoomPrice').val(selectedData.defprice);
        $('#showRoomModal #showRoomType').val(selectedData.type);
        $('#showRoomModal #showRoomNumber').val(selectedData.number);
        $('#showRoomModal #showRoomDescription').val(selectedData.description);
    });

    /**
     * Create new price Interval
     */

    $('#addEventModal #ADDintervalSaveBtn').on('click', function () {

        ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'].forEach(function (val) {
            let thisSelector = '#ADDintervalDaysContainer #ADDinterval_day_' + val;

            if($(thisSelector).val() === 'on') {
                $(thisSelector).attr('checked', 'checked');
                $(thisSelector).val(1)
            } else {
                $(thisSelector).removeAttr('checked');
                $(thisSelector).val(0);
            }
        });

        let bodyData = new FormData(document.forms.addintervalform);
        let arrayData = {};

        bodyData.forEach((data, index) => {
            arrayData[index] = data;
        });

        let url_for_delete = '/api/interval';

        fetch(url_for_delete, {
            method: 'POST',
            headers: {
                'Username': 'api-test-user',
                'Userpassword': 'api-test-password',
            },
            body: bodyData
        })
            .then(function (resp) {
                if (resp.status !== 200) {
                    alert('Произошла ошибка при создании интервала.');
                    console.log('Произошла ошибка при создании интервала. Status Code: #1 ' + resp.status);
                    return;
                }
                resp.json().then(function (data) {
                    console.log('addInterval:', data);
                    if (data.status !== false) {

                        arrayData['id'] = parseInt(data.record_id);
                        arrayData['resourceId'] = parseInt(arrayData['room_id']);
                        arrayData['start'] = moment(arrayData['date_start']);
                        arrayData['end'] = moment(arrayData['date_end']);

                        $(calendarSelector).fullCalendar('renderEvent', arrayData);
                        $(calendarSelector).fullCalendar('rerenderEvents');
                    } else {
                        if (resp.msg) {
                            alert('Произошла ошибка: ' + resp.msg);
                        } else {
                            alert('Произошла ошибка при создании интервала.');
                        }
                        console.log('Произошла ошибка при создании интервала. Status Code: #2 ' + data.status);
                    }
                })
            });
    });

    /**
     * Create new Room
     */

    $('#addRoomModal #ADDRoomSaveBtn').on('click', function () {

        let url_for_delete = '/api/room';
        
        let description = $("#addRoomModal textarea#addRoomDescription").val();
        let bodyData = new FormData(document.forms.addroomform);
        let arrayData = {};

        bodyData.append('description', description);

        bodyData.forEach((data, index) => {
            arrayData[index] = data;
        });

        fetch(url_for_delete, {
            method: 'POST',
            headers: {
                'Username': 'api-test-user',
                'Userpassword': 'api-test-password',
            },
            body: bodyData
        })
            .then(function (resp) {
                if (resp.status !== 200) {
                    alert('Произошла ошибка при добавлении новых апартаментов.');
                    console.log('Произошла ошибка при добавлении новых апартаментов. Status Code: #1 ' + resp.status);
                    return;
                }
                resp.json().then(function (data) {
                    console.log('addInterval:', data);
                    if (data.status !== false) {
                        arrayData['id'] = data.record_id;
                        $(calendarSelector).fullCalendar('addResource', arrayData, true);

                        // $(calendarSelector).fullCalendar('rerenderEvents');
                    } else {
                        if(resp.msg) {
                            alert('Произошла ошибка: ' + resp.msg);
                        } else {
                            alert('Произошла ошибка при добавлении новых апартаментов.');
                        }
                        console.log('Произошла ошибка при добавлении новых апартаментов. Status Code: #2 ' + data.status);
                    }
                })
            });
    });

});
