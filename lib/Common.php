<?php
    /**
     * Created by PhpStorm.
     * User: sergiislobodinskiy
     */

    namespace Lib;

    \defined('RUN') or die('No direct script access.');

    class Common
    {

        /**
         *  Rule for filter / validate ID of record (Room / Interval)
         *
         * @var array
         */

        public static $recordIdRules = [
            'options' => [
                'min_range' => 1
            ]
        ];

        /**
         * Rule for filter / validate Intervals data
         *
         * @var array
         */
        public static $requestInterval = [
             'start' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/'
                ]
            ],
            'end' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/'
                ]
            ]
        ];

        /**
         * Rule for filter / validate overlap check
         *
         * @var array
         */
        public static $requestOverlapCheckStructure = [
            'room_id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1,
                ]
            ],
            'date_start' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/'
                ]
            ],
            'date_end' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/'
                ]
            ],
            'intervalId' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1,
                ]
            ],
        ];

        /**
         * Rule for filter / validate Rooms data
         *
         * @var array
         */
        public static $requestRoomStructure = [
            'title' => FILTER_SANITIZE_STRING,
            'type' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1,
                    'max_range' => 255
                ]
            ],
            'number' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1,
                    'max_range' => 65535
                ]
            ],
            'description' => FILTER_SANITIZE_STRING,
            'defprice' => [
                'filter' => FILTER_VALIDATE_FLOAT,
                'options' => [
                    'min_range' => 0.01
                ]
            ],
        ];

        /**
         * Rule for filter / validate Intervals data
         *
         * @var array
         */
        public static $requestIntervalStructure = [
            'title' => FILTER_SANITIZE_STRING,
            'room_id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ],
            'date_start' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/'
                ]
            ],
            'date_end' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/'
                ]
            ],
            'price' => [
                'filter' => FILTER_VALIDATE_FLOAT,
                'options' => [
                    'min_range' => 0.00
                ]
            ],
            'mon' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 0
                ]
            ],
            'tue' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 0
                ]
            ],
            'wed' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 0
                ]
            ],
            'thu' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 0
                ]
            ],
            'fri' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 0
                ]
            ],
            'sat' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 0
                ]
            ],
            'sun' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 0
                ]
            ]
        ];

        /**
         *  Get all Headers from Request
         *
         * @param array $SERVER
         * 
         * @return array
         */
        public static function getAllHeaders(array $SERVER): array
        {
            $headers = array();

            foreach ($SERVER as $key => $value) {
                if (strpos($key, 'HTTP_') === 0) {
                    $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
                    $headers[$header] = $value;
                }
            }

            return $headers;
        }
    }