<?php
    /**
     * Created by PhpStorm.
     * User: sergiislobodinskiy
     */

    namespace Lib;

    defined('RUN') or die('No direct script access.');

    use Exception;
    use PDO;

    class DBConnector
    {

        protected static $instance;  // object instance
        /**
         * Database name
         * @var string
         */
        public $dbName;
        /**
         * Database host
         * @var string
         */
        public $dbHost;
        /**
         * Database User name
         * @var string
         */
        public $dbUser;
        /**
         * Database User password
         * @var string
         */
        public $dbPassword;
        /**
         * Database PDO connection
         * @var \PDO
         */
        public $connect = null;

        // block "new"
        private function __construct()
        {
        }

        // block cloning
        private function __clone()
        {
        }

        // block unserialize
        private function __wakeup()
        {
        }

        // get/check Instance of Singleton
        public static function getInstance($dbHost, $dbName, $dbUser, $dbPassword)
        {
            if (is_null(self::$instance)) {
                self::$instance = new DBConnector();
                self::$instance->dbName = $dbName;
                self::$instance->dbHost = $dbHost;
                self::$instance->dbUser = $dbUser;
                self::$instance->dbPassword = $dbPassword;
                self::$instance->openConnection();
            }

            return self::$instance;
        }

        // Connect to DataBase
        public function openConnection()
        {
            if (is_null($this->connect)) {
                try {
                    $this->connect = new PDO('mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName, $this->dbUser, $this->dbPassword);
                } catch (Exception $e) {
                    echo "ERROR: Connection fail!";
                    $this->connect = null;
                    return false;
                }

            }

            return $this->connect;
        }

        // Connection close
        public function closeConnection()
        {
            if (!is_null($this->connect)) {
                $this->connect = null;
            }
        }

    }