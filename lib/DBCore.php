<?php
    /**
     * Created by PhpStorm.
     * User: sergiislobodinskiy
     */

    namespace Lib;

    \defined('RUN') or die('No direct script access.');

    use Exception;

    class DBCore extends \FluentPDO
    {

        private $roomTableName = 'room';
        private $intervalTableName = 'room_interval';


        /**
         * DBCore constructor.
         *
         * @param \PDO $PDO
         */
        public function __construct(\PDO $PDO)
        {
            parent::__construct($PDO);
        }

        /**
         * Add new Room to DB
         *
         * @param $values
         *   array('type' => 2, 'number' => 56, 'description' => 'Эконом класса')
         *
         * @return int | boolean
         */
        public function addRoom(array $values)
        {
            try {
                $query = $this->insertInto($this->roomTableName)
                    ->values($values);
                $status = $query->execute();
            } catch (Exception $e) {
                $status = false;
            }

            return $status;
        }

        /**
         *  Get all Rooms Data
         *
         * @return array | boolean
         */
        public function getAllRooms() : array
        {
            try {
                $queryData = $this->from($this->roomTableName)
                    ->fetchAll();

            } catch (Exception $e) {
                $queryData = false;
            }

            return $queryData;
        }

        /**
         *  Get Room Data by $id
         *
         * @param $id
         *
         * @return array | boolean
         */
        public function getRoom(int $id) : array 
        {
            try {
                $queryData = $this->from($this->roomTableName)
                    ->where('id', $id)
                    ->fetchAll();

            } catch (Exception $e) {
                $queryData = false;
            }

            return $queryData;
        }

        /**
         *  UPDATE Room Data
         *
         * @param int $id
         *
         * @param array $data
         *
         * @return int | boolean
         */
        public function updateRoom(int $id, array $data): int
        {

            try {
                $status = $this->update($this->roomTableName)
                    ->set($data)
                    ->where('id', $id)
                    ->execute();

            } catch (Exception $e) {
                $status = false;
            }

            return $status;                                 
        }

        /**
         *  Delete Room Data by $id
         *
         * @param $id
         *
         * @return int | boolean
         */
        public function deleteRoom(int $id) : int
        {

            try {
                $status = $this->deleteFrom($this->roomTableName)
                    ->where('id', $id)
                    ->execute();

            } catch (Exception $e) {
                $status = false;
            }

            return $status;
        }

        /**
         *  Check Overlap
         *
         * @param int $roomId
         * @param string $startDate
         * @param string $endDate
         *
         * @param int $intervalSelfId
         * @return array | boolean
         */
        public function checkOverlap(int $roomId, string $startDate, string $endDate, int $intervalSelfId = null)
        {
            try {
                $queryData = $this->from($this->intervalTableName)
                    ->select(null)
                    ->select('id, title, room_id AS resourceId, date_start AS start, date_end AS end')
                    ->where('date_end > ?',  $startDate)
                    ->where('date_start < ?',  $endDate)
                    ->where('room_id = ?', $roomId)
                    ->where('id <> ?', $intervalSelfId)
//                    ->where('NOT ((date_end = ? AND date_start <> ?) OR (date_end <> ?  AND date_start = ?))', $startDate, $endDate, $startDate, $endDate)
                    ->fetchAll();
            } catch (Exception $e) {
                echo 'ERROR: Fetching all data for ' . $this->intervalTableName;
                $queryData = false;
            }

            return $queryData;
        }

        /**
         *  Get all Intervals Data
         *
         * @param string $startDate
         * @param string $endDate
         *
         * @return array | boolean
         */
        public function getAllIntervals(string $startDate = null, string $endDate = null)
        {
            try {
                $queryData = $this->from($this->intervalTableName)
                    ->select(null)
                    ->select('id, title, room_id AS resourceId, date_start AS start, date_end AS end, price, mon, tue, wed, thu, fri, sat, sun')
                    ->where('date_start >= ?',  $startDate)
                    ->where('date_end <= ?',  $endDate)
                    ->fetchAll();

            } catch (Exception $e) {
                echo 'ERROR: Fetching all data for ' . $this->intervalTableName;
                $queryData = false;
            }

            return $queryData;
        }

        /**
         *  Get Price interval Data by $id
         *
         * @param $id
         *
         * @return array | boolean
         */
        public function getInterval(int $id)
        {
            try {
                $queryData = $this->from($this->intervalTableName)
                    ->where('id', $id)
                    ->fetchAll();

            } catch (Exception $e) {
                echo 'ERROR: Fetching data for ' . $this->intervalTableName;
                $queryData = false;
            }

            return $queryData;
        }

        /**
         * Add new Room Price Interval to DB
         *
         * @param $values
         *   array('room_id' => 2, 'date_start' => '2018-09-10', 'date_end' => '2018-09-12', 'price' => 100.20, 'mon' => 1, 'tue' => 1, 'wed' => 1, 'thu' => 1, 'fri' => 1, 'sat' => 1,  'sun' => 1)
         *
         * @return array | boolean
         */
        public function addInterval(array $values)
        {
            try {
                $query = $this->insertInto($this->intervalTableName)
                    ->values($values);
                $status = $query->execute();
            } catch (Exception $e) {
                echo 'ERROR: Inserting new room';
                $status = false;
            }

            return $status;
        }

        /**
         *  UPDATE price interval Data
         *
         * @param int $id
         *
         * @param array $data
         *
         * @return array | boolean
         */
        public function updateInterval(int $id, array $data)
        {

            try {
                $status = $this->update($this->intervalTableName)
                    ->set($data)
                    ->where('id', $id)
                    ->execute();

            } catch (Exception $e) {
                echo 'ERROR: Updating data for ' . $this->intervalTableName;
                $status = false;
            }

            return $status;
        }

        /**
         *  Delete price interval data by $id
         *
         * @param $id
         *
         * @return int | boolean
         */
        public function deleteInterval(int $id)
        {

            try {
                $status = $this->deleteFrom($this->intervalTableName)
                    ->where('id', $id)
                    ->execute();

            } catch (Exception $e) {
                echo 'ERROR: Deleting data for ' . $this->intervalTableName;
                $status = false;
            }

            return $status;
        }

    }