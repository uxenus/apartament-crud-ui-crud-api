<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Api extends \Codeception\Module
{
    public function getNewRecordIdFromResponce(){
        $res = json_decode($this->getModule('REST')->response, true);
        return $res['record_id'];
    }
}
