-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.20 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных gillsoft
CREATE DATABASE IF NOT EXISTS `gillsoft` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gillsoft`;

-- Дамп структуры для таблица gillsoft.room
CREATE TABLE IF NOT EXISTS `room` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'Room',
  `type` tinyint(1) unsigned NOT NULL,
  `number` smallint(3) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `defprice` float unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы gillsoft.room: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT IGNORE INTO `room` (`id`, `title`, `type`, `number`, `description`, `defprice`) VALUES
	(1, 'Room 1', 5, 56, 'Эконом класса', 50),
	(2, 'Room 2', 7, 777, 'Lux class', 300),
	(12, 'Комната среднего уровня', 5, 220, 'Хороший номер для семьи.', 137),
	(29, 'Номер Люкс', 1, 17, 'Лучший номер в отеле!', 300);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;

-- Дамп структуры для таблица gillsoft.room_interval
CREATE TABLE IF NOT EXISTS `room_interval` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `price` float unsigned NOT NULL,
  `mon` tinyint(3) unsigned NOT NULL,
  `tue` tinyint(3) unsigned NOT NULL,
  `wed` tinyint(3) unsigned NOT NULL,
  `thu` tinyint(3) unsigned NOT NULL,
  `fri` tinyint(3) unsigned NOT NULL,
  `sat` tinyint(3) unsigned NOT NULL,
  `sun` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы gillsoft.room_interval: ~13 rows (приблизительно)
/*!40000 ALTER TABLE `room_interval` DISABLE KEYS */;
INSERT IGNORE INTO `room_interval` (`id`, `title`, `room_id`, `date_start`, `date_end`, `price`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `sun`) VALUES
	(40, 'Праздник', 2, '2018-09-07', '2018-09-09', 100, 1, 1, 1, 1, 1, 1, 1),
	(43, 'Скидка -20%', 2, '2018-09-10', '2018-09-13', 80, 1, 1, 1, 1, 1, 1, 1),
	(44, 'Стандарт цена -10%', 12, '2018-09-04', '2018-09-10', 140, 0, 0, 0, 0, 0, 0, 0),
	(45, 'Сток', 1, '2018-09-03', '2018-09-04', 50, 0, 0, 0, 0, 0, 0, 0),
	(46, 'Сток', 1, '2018-09-06', '2018-09-07', 50, 0, 0, 0, 0, 0, 0, 0),
	(47, 'Сток', 1, '2018-09-14', '2018-09-16', 50, 0, 0, 0, 0, 0, 0, 0),
	(48, 'Акция', 29, '2018-09-06', '2018-09-08', 250, 1, 1, 1, 1, 1, 1, 1);
/*!40000 ALTER TABLE `room_interval` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
