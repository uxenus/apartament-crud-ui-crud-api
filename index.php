<?php

    /**
     * Created by PhpStorm.
     * User: sergiislobodinskiy
     */

    error_reporting(E_ALL | E_STRICT);

    define('RUN', true);

    require __DIR__ . '/vendor/autoload.php';

    use Lib\Common;
    use Lib\DBConnector;
    use Lib\DBCore;


    /**
     * Get all headers
     */
    $headersIN = Common::getAllHeaders($_SERVER);
    $httpMethod = $_SERVER['REQUEST_METHOD'];
    $uri = $_SERVER['REQUEST_URI'];

    if (false !== $pos = strpos($uri, '?')) {
        $uri = substr($uri, 0, $pos);
    }

    $uri = rawurldecode($uri);

    /**
     * Response body
     */
    $responseBody = '';
    $routMODE = '';
    $errorMessage = '';
    
    /* ========================================================================== */

/**
     * Routing
     */
    $dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
        $r->addRoute('OPTIONS', '/api', 'service');
        /* ----------------------------------------------------------------------------------*/
        $r->addRoute('GET', '/', 'mainPage');
        /* ============================================= */
        $r->addRoute('GET', '/api/room/{id:\d+}', 'getRoom');
        $r->addRoute('POST', '/api/rooms', 'getAllRooms');
        $r->addRoute('POST', '/api/room', 'addRoom');
        $r->addRoute('POST', '/api/room/{id:\d+}', 'updateRoom');
        $r->addRoute('DELETE', '/api/room/{id:\d+}', 'deleteRoom');

        $r->addRoute('GET', '/api/interval/{id:\d+}', 'getInterval');
        $r->addRoute('POST', '/api/intervals', 'getAllIntervals');
        $r->addRoute('POST', '/api/interval', 'addInterval');
        $r->addRoute('POST', '/api/interval/{id:\d+}', 'updateInterval');
        $r->addRoute('DELETE', '/api/interval/{id:\d+}', 'deleteInterval');
        $r->addRoute('POST', '/api/interval/checkoverlap', 'checkOverlap');

    });

    $routeInfo = $dispatcher->dispatch($httpMethod, $uri);

    switch ($routeInfo[0]) {
        case FastRoute\Dispatcher::NOT_FOUND:
            $statusCode = '404 Not Found';
            $responseBody = ['status' => $statusCode];
            break;
        case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
            $statusCode = '405 Method Not Allowed';
            $responseBody = ['status' => $statusCode];
            break;
        case FastRoute\Dispatcher::FOUND:

            $routMODE = $routeInfo[1];

            /**
             * DB connection
             */
            if ($routMODE !== 'service' && $routMODE !== 'mainPage') {
                $DB = DBConnector::getInstance('localhost', 'gillsoft', 'gillsoft_user', '123');
                $dbCore = new DBCore($DB->connect);

                if (!isset($headersIN['Username'], $headersIN['Userpassword']) || $headersIN['Username'] !== 'api-test-user' || $headersIN['Userpassword'] !== 'api-test-password') {
                    $statusCode = '401 Unauthorized';
                    $responseBody = ['status' => $statusCode];

                    $routeInfo[1] = null;
                }
            }

            /* ========================================================================== */

            switch ($routeInfo[1]) {
                
                /*------------------------------------------------------------ */
                case 'service':

                    $headersOUT = [
                        'Access-Control-Allow-Origin' => '*',
                        'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
                        'Access-Control-Allow-Credentials' => 'true',
                        'Access-Control-Max-Age' => '86400',
                        'Access-Control-Allow-Headers' => 'Content-Type, Authorization, X-Requested-With',
                        'Allow' => 'POST, GET, OPTIONS, PUT, DELETE'
                    ];

                    foreach ($headersOUT as $headerName => $headerValue) {
                        header($headerName . ': ' . $headerValue);
                    }

                    $statusCode = '200 OK';
                    $responseBody = [
                        'method' => 'OPTIONS'
                    ];
                    break;

                /*------------------------------------------------------------ */

                case 'getAllRooms':
                    /**
                     * Get all rooms data
                     */

                    $result = $dbCore->getAllRooms();

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'roomsData' => $result
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /*------------------------------------------------------------ */
                case 'addRoom':
                    /**
                     * Create new Room
                     */

                    $addData = filter_input_array(INPUT_POST, Common::$requestRoomStructure, false);

                    if(!empty($addData)) {
                        $result = $dbCore->addRoom($addData);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'record_id' => $result
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /*------------------------------------------------------------ */
                case 'getRoom':
                    /**
                     * Get room data
                     */

                    $roomID = filter_var($routeInfo[2]['id'], FILTER_VALIDATE_INT, Common::$recordIdRules);

                    if(is_numeric($roomID)) {
                        $result = $dbCore->getRoom($roomID);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }
                    
                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'roomData' => $result
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /*------------------------------------------------------------ */
                case 'updateRoom':
                    /**
                     * Update new Room
                     */

                    $roomID = filter_var($routeInfo[2]['id'], FILTER_VALIDATE_INT, Common::$recordIdRules);

                    $updateData = filter_input_array(INPUT_POST, Common::$requestRoomStructure, false);

                    if(!empty($updateData) && is_numeric($roomID)) {
                        $result = $dbCore->updateRoom($roomID, $updateData);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'updatedId' => $roomID
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /*------------------------------------------------------------ */
                case 'deleteRoom':
                    /**
                     * Delete room data
                     */

                    $roomID = filter_var($routeInfo[2]['id'], FILTER_VALIDATE_INT, Common::$recordIdRules);

                    if(is_numeric($roomID)) {
                        $result = $dbCore->deleteRoom($roomID);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'deletedId' => $roomID
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /* ===================================================== */

                case 'checkOverlap':
                    /**
                     * Check Overlap of records
                     */

                    $overlapData = filter_input_array(INPUT_POST, Common::$requestOverlapCheckStructure, false);

                    if(!empty($overlapData)) {
                        $result = $dbCore->checkOverlap($overlapData['room_id'], $overlapData['date_start'], $overlapData['date_end']);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'overlapping' => true,
                            'overlapData' => $result
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }

                    break;
                /*------------------------------------------------------------ */
                case 'getAllIntervals':
                    /**
                     * Get all intervals data
                     */

                    $addData = filter_input_array(INPUT_POST, Common::$requestInterval);

                    if(!empty($addData)) {
                        $dateStart = (new DateTime($addData['start']))->format('Y-m-d');
                        $dateEnd = (new DateTime($addData['end']))->format('Y-m-d');

                        $result = $dbCore->getAllIntervals($dateStart, $dateEnd);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'intervalsData' => $result
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /*------------------------------------------------------------ */
                case 'getInterval':
                    $intervalID = filter_var($routeInfo[2]['id'],  FILTER_VALIDATE_INT, Common::$recordIdRules);


                    if(is_numeric($intervalID)) {
                        $result = $dbCore->getInterval($intervalID);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'intervalData' => $result
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /*------------------------------------------------------------ */
                case 'addInterval':

                    /**
                     * Create new price Interval
                     */

                    $addData = filter_input_array(INPUT_POST, Common::$requestIntervalStructure, false);

                    if(!empty($addData)) {

                        $week = [
                            'mon' => 0,
                            'tue' => 0,
                            'wed' => 0,
                            'thu' => 0,
                            'fri' => 0,
                            'sat' => 0,
                            'sun' => 0
                        ];
                        $addData = array_merge($week, $addData);

                        $overlapResults = $dbCore->checkOverlap($addData['room_id'], $addData['date_start'], $addData['date_end']);
                        if(empty($overlapResults)) {
                            $result = $dbCore->addInterval($addData);
                        } else {
                            $errorMessage = 'Overlap';
                            $result = false;
                        }

                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'record_id' => $result
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }

                    break;

                /*------------------------------------------------------------ */
                case 'updateInterval':
                    /**
                     * Update price Interval
                     */

                    $intervalID = filter_var($routeInfo[2]['id'], FILTER_VALIDATE_INT, Common::$recordIdRules);

                    $updateData = filter_input_array(INPUT_POST, Common::$requestIntervalStructure, false);

                    if(!empty($updateData) && is_numeric($intervalID)) {

                        $week = [
                            'mon' => 0,
                            'tue' => 0,
                            'wed' => 0,
                            'thu' => 0,
                            'fri' => 0,
                            'sat' => 0,
                            'sun' => 0
                        ];
                        $updateData = array_merge($week, $updateData);

                        if (!empty($updateData)) {
                            $overlapResults = $dbCore->checkOverlap($updateData['room_id'], $updateData['date_start'], $updateData['date_end'], $intervalID);

                            if (empty($overlapResults)) {
                                $result = $dbCore->updateInterval($intervalID, $updateData);
                            } else {
                                $errorMessage = ' Overlap';
                                $result = false;
                            }
                        } else {
                            $errorMessage = 'Invalid request data';
                            $result = false;
                        }


                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'updatedId' => $intervalID
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;

                /*------------------------------------------------------------ */
                case 'deleteInterval':
                    /**
                     * Delete interval data
                     */

                    $intervalID = filter_var($routeInfo[2]['id'], FILTER_VALIDATE_INT, Common::$recordIdRules);

                    if(is_numeric($intervalID)) {
                        $result = $dbCore->deleteInterval($intervalID);
                    } else {
                        $errorMessage = 'Invalid request data';
                        $result = false;
                    }

                    $statusCode = '200 OK';
                    if ($result && !empty($result)) {
                        $responseBody = [
                            'status' => true,
                            'deletedId' => $intervalID
                        ];
                    } else {
                        $responseBody = [
                            'status' => false,
                            'msg' => $errorMessage
                        ];
                    }
                    break;
            }

            break;
    }

    /* =========================================== */

if ($routMODE === 'mainPage') {
    header('Content-Type: text/html; charset=UTF-8');
    echo file_get_contents('index.html');
} else {
    header('Content-Type: application/json; charset=UTF-8');
    header('HTTP/1.1 ' . $statusCode);
    echo json_encode($responseBody);
}

